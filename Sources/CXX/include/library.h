//
// Created by sortofsleepy on 6/8/2023.
//

#ifndef SWIFT_CMAKE2_LIBRARY_H
#define SWIFT_CMAKE2_LIBRARY_H

namespace library {
    struct Version {
        int major;
        int minor;
        int patch;
    };

    Version version() noexcept;
}

#endif //SWIFT_CMAKE2_LIBRARY_H
